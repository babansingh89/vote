﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using dash.Connection;
using dash.DAL;
using System.Data;
using dash.Models;
using dash.AppCode;

namespace VotingDashboard.Controllers
{
    public class AdminHomeController : Controller
    {
        string ConnectionString = null;
        MySqlConnection cn = null;
        HomeDAL dal = new HomeDAL();
        ClientJsonResult cr = new ClientJsonResult();
        public AdminHomeController()
        {
            ConnectionString = dbConnection.GetConnectionString();
            cn = new MySqlConnection(ConnectionString);
        }
        // GET: AdminHome
        public ActionResult Index()
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Datagridactivity(string myDate, string ActivityID)
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            try
            {
                string userid = Convert.ToString(Session["session_userid"]);
                DataSet ds = dal.dataactivityDAL(myDate, userid, ActivityID);
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", 1);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}