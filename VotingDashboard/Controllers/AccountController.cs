﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using dash.Models;
using System.Data;
using MySql.Data.MySqlClient;
using dash.DAL;
using dash.AppCode;
using dash.Connection;

namespace dash.Controllers
{
    public class AccountController : Controller
    {
        string ConnectionString = null;
        MySqlConnection cn = null;
        HomeDAL dal = new HomeDAL();
        ClientJsonResult cr = new ClientJsonResult();
        public AccountController()
        {
            ConnectionString = dbConnection.GetConnectionString();
            cn = new MySqlConnection(ConnectionString);
        }
        
        // GET: /Account/Login
        public ActionResult Login()
        {
            if (Convert.ToInt32(Session["session_userid"]) > 0 || Session["session_userid"] != null)
            {
                Response.Redirect("/Home/Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult SubmitLogin(string Username, string Password)
        {
            try
            {
                DataSet ds = dal.SubmitLoginDAL(Username, Password);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Session["session_userid"] = ds.Tables[0].Rows[0]["userid"];
                        Session["session_name"] = ds.Tables[0].Rows[0]["name"];
                        Session["session_username"] = ds.Tables[0].Rows[0]["username"];
                        Session["session_email"] = ds.Tables[0].Rows[0]["email"];
                        Session["session_mobile"] = ds.Tables[0].Rows[0]["mobile"];
                        Session["session_user_type_code"] = ds.Tables[0].Rows[0]["user_type_code"];
                    }
                }
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", 1);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        public void Logout()
        {
            Session.Abandon();
            Response.Redirect("/Account/Login");
        }




    }
}