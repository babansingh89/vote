﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using dash.Connection;
using dash.DAL;
using System.Data;
using dash.Models;
using dash.AppCode;

namespace dash.Controllers
{
    public class HomeController : Controller
    {
        string ConnectionString = null;
        MySqlConnection cn = null;
        HomeDAL dal = new HomeDAL();
        ClientJsonResult cr = new ClientJsonResult();

        public HomeController() {
            ConnectionString = dbConnection.GetConnectionString();
            cn = new MySqlConnection(ConnectionString);
        }

        public ActionResult Index()
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            return View();
        }

        [HttpPost]
        public ActionResult SegmentInsidedata(string myDate, string SegmentID, string ReportID, string p_parameter_one, string p_parameter_two, string p_parameter_three, string p_parameter_four, string p_parameter_five)
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            try
            {
                string userid = Convert.ToString(Session["session_userid"]);
                DataSet ds = dal.Load_segmentDAL(myDate, userid, SegmentID, ReportID,  p_parameter_one,  p_parameter_two,  p_parameter_three,  p_parameter_four,  p_parameter_five);
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", 1);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }


        [HttpPost]
        public ActionResult DashboardDate(string strDate)
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            try
            {
                DataSet ds = dal.GetDashboardRPT(strDate);
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", 1);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult activityDetailCountDistrictWise(string activityTypeID, string activityDetailID, string DistrictID, string inputDate)
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            try
            {
                List<DistrictDetailMD> lst = dal.activityDetailCountDistrictWiseDAL(activityTypeID, activityDetailID, DistrictID, inputDate);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", 1);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult activityDetailCountConstituencyWise(string activityTypeID, string activityDetailID, string ConstituencyID, string inputDate)
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            try
            {
                List<ConstituencyDetailMD> lst = dal.activityDetailCountConstituencyWiseDAL(activityTypeID, activityDetailID, ConstituencyID, inputDate);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", 1);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult activityDetailCountBoothWise(string activityTypeID, string activityDetailID, string boothID, string inputDate)
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            try
            {
                List<BoothDetailMD> lst = dal.activityDetailCountBoothWiseDAL(activityTypeID, activityDetailID, boothID, inputDate);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", 1);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult FactSheetData(string transname,string DistrictId, string ContituencyID, string BoothID,string PCID,string p_Date)
        {
            if (Convert.ToInt32(Session["session_userid"]) <= 0 || Session["session_userid"] == null)
            {
                Response.Redirect("/Account/Login");
            }
            try
            {
                string userid = Convert.ToString(Session["session_userid"]);
                DataSet ds = dal.Factsheetdata_DAL(transname,DistrictId, ContituencyID, userid, BoothID, PCID, p_Date);
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = ds.Tables.Count.ToString();
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }





        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}