﻿using dash.Connection;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using dash.Models;

namespace dash.DAL
{
    public class HomeDAL
    {
        string ConnectionString = null;
        MySqlConnection cn = null;
        public HomeDAL()
        {
            ConnectionString = dbConnection.GetConnectionString();
            cn = new MySqlConnection(ConnectionString);
        }

        public List<DashboardMD> Load_segmentDAL()
        {
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.load_segment", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("p_searchDate", strDate == "null" || strDate == "" ? DBNull.Value : (object)strDate);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                List<DashboardMD> lst = new List<DashboardMD>();
                lst = ds.Tables[0].AsEnumerable().Select(t =>
                {
                    var obj = new DashboardMD();
                    obj.SegmentID = Convert.ToInt32(t["SegmentID"]);
                    obj.SegmentName = Convert.ToString(t["SegmentName"]);
                    obj.isSelected = Convert.ToInt32(t["isSelected"]);
                    obj.PanelType = Convert.ToString(t["PanelType"]);
                    obj.PanelSide = Convert.ToString(t["PanelSide"]);
                    return obj;
                }).Where(a => a.PanelType == "T").ToList();
                return lst;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }

        public DataSet Load_segmentDAL(string myDate, string userid, string SegmentID,string ReportID, string p_parameter_one, string p_parameter_two, string p_parameter_three, string p_parameter_four, string p_parameter_five)
        {
            string rptId = "";
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.rpt_dynamic_db_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_myDate", myDate == "null" || myDate == "" ? DBNull.Value : (object)myDate);
            cmd.Parameters.AddWithValue("p_userid", userid == "null" || userid == "" ? DBNull.Value : (object)userid);
            cmd.Parameters.AddWithValue("p_SegmentID", SegmentID == "null" || SegmentID == "" ? DBNull.Value : (object)SegmentID);
            cmd.Parameters.AddWithValue("p_c_rptID", ReportID == "null" || ReportID == "" ? DBNull.Value : (object)ReportID);
            cmd.Parameters.AddWithValue("p_parameter_one", p_parameter_one == "null" || p_parameter_one == "" ? DBNull.Value : (object)p_parameter_one);
            cmd.Parameters.AddWithValue("p_parameter_two", p_parameter_two == "null" || p_parameter_two == "" ? DBNull.Value : (object)p_parameter_two);
            cmd.Parameters.AddWithValue("p_parameter_three", p_parameter_three == "null" || p_parameter_three == "" ? DBNull.Value : (object)p_parameter_three);
            cmd.Parameters.AddWithValue("p_parameter_four", p_parameter_four == "null" || p_parameter_four == "" ? DBNull.Value : (object)p_parameter_four);
            cmd.Parameters.AddWithValue("p_parameter_five", p_parameter_five == "null" || p_parameter_five == "" ? DBNull.Value : (object)p_parameter_five);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                //List<DashboardMD> lst = new List<DashboardMD>();
                //lst = ds.Tables[0].AsEnumerable().Select(t =>
                //{
                //    var obj = new DashboardMD();
                //    obj.SegmentID = Convert.ToString(t["SegmentID"]);
                //    obj.SegmentName = Convert.ToString(t["SegmentName"]);
                //    obj.isSelected = Convert.ToString(t["isSelected"]);
                //    return obj;
                //}).ToList();
                return ds;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }



        ////////////////////////////////////////////////////////////
        public DataSet SubmitLoginDAL(string Username, string Password)
        {
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.user_login_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_username", Username == "null" || Username == "" ? DBNull.Value : (object)Username);
            cmd.Parameters.AddWithValue("p_password", Password == "null" || Password == "" ? DBNull.Value : (object)Password);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }

        public DataSet GetDashboardRPT(string strDate)
        {
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.dashboardrpt_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_searchDate", strDate == "null" || strDate == "" ? DBNull.Value : (object)strDate);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                //List<DashboardRPTMD> lst = new List<DashboardRPTMD>();
                //lst = ds.Tables[0].AsEnumerable().Select(t =>
                //{
                //    var obj = new DashboardRPTMD();
                //    obj.District = Convert.ToString(t["District"]);
                //    obj.Constituency = Convert.ToString(t["Constituency"]);
                //    obj.Booth = Convert.ToString(t["Booth"]);
                //    return obj;
                //}).ToList();
                return ds;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }

        public List<DistrictDetailMD> activityDetailCountDistrictWiseDAL(string activityTypeID, string activityDetailID, string DistrictID, string inputDate)
        {
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.activityDetailCountDistrictWise_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_activityTypeID", activityTypeID == "null" || activityTypeID == "" ? DBNull.Value : (object)activityTypeID);
            cmd.Parameters.AddWithValue("p_activityDetailID", activityDetailID == "null" || activityDetailID == "" ? DBNull.Value : (object)activityDetailID);
            cmd.Parameters.AddWithValue("p_DistrictID", DistrictID == "null" || DistrictID == "" ? DBNull.Value : (object)DistrictID);
            cmd.Parameters.AddWithValue("p_inputDate", inputDate == "null" || inputDate == "" ? DBNull.Value : (object)inputDate);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                List<DistrictDetailMD> lst = new List<DistrictDetailMD>();
                lst = ds.Tables[0].AsEnumerable().Select(t =>
                {
                    var obj = new DistrictDetailMD();
                    obj.CNT = Convert.ToString(t["CNT"]);
                    obj.activitytype = Convert.ToString(t["activitytype"]);
                    obj.districtname = Convert.ToString(t["districtname"]);
                    return obj;
                }).ToList();
                return lst;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }

        public List<ConstituencyDetailMD> activityDetailCountConstituencyWiseDAL(string activityTypeID, string activityDetailID, string ConstituencyID, string inputDate)
        {
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.activityDetailCountConstituencyWise_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_activityTypeID", activityTypeID == "null" || activityTypeID == "" ? DBNull.Value : (object)activityTypeID);
            cmd.Parameters.AddWithValue("p_activityDetailID", activityDetailID == "null" || activityDetailID == "" ? DBNull.Value : (object)activityDetailID);
            cmd.Parameters.AddWithValue("p_ConstituencyID", ConstituencyID == "null" || ConstituencyID == "" ? DBNull.Value : (object)ConstituencyID);
            cmd.Parameters.AddWithValue("p_inputDate", inputDate == "null" || inputDate == "" ? DBNull.Value : (object)inputDate);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                List<ConstituencyDetailMD> lst = new List<ConstituencyDetailMD>();
                lst = ds.Tables[0].AsEnumerable().Select(t =>
                {
                    var obj = new ConstituencyDetailMD();
                    obj.CNT = Convert.ToString(t["CNT"]);
                    obj.activitytype = Convert.ToString(t["activitytype"]);
                    obj.constituencyname = Convert.ToString(t["constituencyname"]);
                    return obj;
                }).ToList();
                return lst;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }

        public List<BoothDetailMD> activityDetailCountBoothWiseDAL(string activityTypeID, string activityDetailID, string boothID, string inputDate)
        {
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.activityDetailCountBoothWise_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_activityTypeID", activityTypeID == "null" || activityTypeID == "" ? DBNull.Value : (object)activityTypeID);
            cmd.Parameters.AddWithValue("p_activityDetailID", activityDetailID == "null" || activityDetailID == "" ? DBNull.Value : (object)activityDetailID);
            cmd.Parameters.AddWithValue("p_boothID", boothID == "null" || boothID == "" ? DBNull.Value : (object)boothID);
            cmd.Parameters.AddWithValue("p_inputDate", inputDate == "null" || inputDate == "" ? DBNull.Value : (object)inputDate);
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                List<BoothDetailMD> lst = new List<BoothDetailMD>();
                lst = ds.Tables[0].AsEnumerable().Select(t =>
                {
                    var obj = new BoothDetailMD();
                    obj.CNT = Convert.ToString(t["CNT"]);
                    obj.activitytype = Convert.ToString(t["activitytype"]);
                    obj.boothname = Convert.ToString(t["boothname"]);
                    return obj;
                }).ToList();
                return lst;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }


        public DataSet dataactivityDAL(string myDate, string userid, string ActivityID)
        {
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.load_emergency_list", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_activityTypeID", ActivityID == "null" || ActivityID == "" ? DBNull.Value : (object)ActivityID);
            cmd.Parameters.AddWithValue("p_searchDate", myDate == "null" || myDate == "" ? DBNull.Value : (object)myDate);
            cmd.Parameters.AddWithValue("p_userID", userid == "null" || userid == "" ? DBNull.Value : (object)userid);
           
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
               
                return ds;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }


        public DataSet Factsheetdata_DAL(string transname,string DistrictId,  string ContituencyID, string userid, string BoothID ,string PCID, string p_Date)
        {
            MySqlCommand cmd = new MySqlCommand("db_boothmanagement.factSheet_sp", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_transname", transname == "null" || transname == "" ? DBNull.Value : (object)transname);
            cmd.Parameters.AddWithValue("p_districtID", DistrictId == "null" || DistrictId == "" ? DBNull.Value : (object)DistrictId);
            cmd.Parameters.AddWithValue("p_constituencyId", ContituencyID == "null" || ContituencyID == "" ? DBNull.Value : (object)ContituencyID);
            cmd.Parameters.AddWithValue("p_boothid", BoothID == "null" || BoothID == "" ? DBNull.Value : (object)BoothID);
            cmd.Parameters.AddWithValue("p_PCID", PCID == "null" || PCID == "" ? DBNull.Value : (object)PCID);
            cmd.Parameters.AddWithValue("p_userid", userid == "null" || userid == "" ? DBNull.Value : (object)userid);
            cmd.Parameters.AddWithValue("p_searchDate", p_Date == "null" || p_Date == "" ? DBNull.Value : (object)p_Date);

            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);

                return ds;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally { cn.Close(); }
        }



    }
}