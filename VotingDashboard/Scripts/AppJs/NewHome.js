﻿var _selectDate = '';



$(document).ready(function () {
    Object.defineProperty(Array.prototype, 'chunk', {
        value: function (chunkSize) {
            var R = [];
            for (var i = 0; i < this.length; i += chunkSize)
                R.push(this.slice(i, i + chunkSize));
            return R;
        }
    });

    $('#txtdate').mask("9999-99-99", { placeholder: "_" });
    $('#txtdate').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'yy-mm-dd',
        onSelect: function (dateText, inst) {
            BindTopPanel();
           //BindSegment();
            BindRightNotification();
            BindSegmentClick(1);
        }
    });
    $("#txtdate").datepicker().datepicker("setDate", new Date());
    BindTopPanel();
    BindRightNotification();
    BindSegmentClick(1);
   // $("#tab").tabs("select", ".chtb_1");
    setInterval(function () {
        BindTopPanel()
    }, 120000)
    $('.chtbtmv').removeClass('active');
    $('.chtb_1').addClass('active');
   
    //$('#tab_3').removeClass('active');
    _selectDate = $.trim($('#txtdate').val());

    $('#dtdata').on('change', function () {
        //alert(this.value);
        if (this.value != '') {
            _selectDate = this.value;
            BindTopPanel();

          
            //BindSegment();
            BindRightNotification();
            BindSegmentClick(1);
            $('.chtbtmv').removeClass('active');
            $('.chtb_1').addClass('active');
        } else {
            alert('Please Select any Valid date');
        }
    });
    get_dist_autocomp();
    get_dist_autocomp1()

    $("#txt_district1").change(function () {
        $("#txt_Constituency1").val('');
        $("#constituency_id1").val('');
        $("#txt_booth1").val('');
        $("#booth_id1").val('');
    })
});


function BindTopPanel() {
    var Date = _selectDate; // $.trim($('#txtdate').val());
    var SegmentID = 4;
    var ReportID = 0; var p_parameter_one = 0; p_parameter_two = 0; var p_parameter_three = 0; var p_parameter_four = ''; var p_parameter_five = '';

  //  $('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "',ReportID:'" + ReportID + "',p_parameter_one:'" + p_parameter_one + "',p_parameter_two:'" + p_parameter_two + "',p_parameter_three:'" + p_parameter_three + "',p_parameter_four:'" + p_parameter_four + "',p_parameter_five:'" + p_parameter_five + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var _table = []; var arrlabel = ['completedCnt', 'PendingCount'];
            var arrdata = []; var arrcoloR = ['#208eff', '#ffa55c'];
            var div = '';


            var Table = xml.find("Table");
            var Table1 = xml.find("Table1");
            var Table2 = xml.find("Table2");
            var Table3 = xml.find("Table3");
            var Table4 = xml.find("Table4");

            _table = [Table, Table1, Table2, Table3, Table4];

            $('#4').html('');
            for (var i = 0; i < _table.length; ++i) {
                var rptid = $.trim($(_table[i]).find("rptid").text());
                var ReportName = $.trim($(_table[i]).find("ReportName").text());
                var ChildRptID = $.trim($(_table[i]).find("ChildRptID").text());

                var isDetail = ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;">  </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="TopPanelDetail(' + ChildRptID + ','+ SegmentID+')">More info <i class="fas fa-arrow-circle-right"></i></a>';


                if (rptid == '7') {
                    div += '<div class="col-lg-3 col-6">';
                    div += '<div class="small-box bg-info">';
                    div += '<div class="inner"><h3>' + $.trim($(_table[i]).find("contituency").text()) + '</h3><p>' + ReportName +'</p></div >';
                    div += '<div class="icon"><i class="fas fa-users" ></i ></div>' + isDetail+'</div ></div>';
                    
                }
                if (rptid == '8') {
                    div += '<div class="col-lg-3 col-6">';
                    div += '<div class="small-box bg-success">';
                    div += '<div class="inner"><h3>' + $.trim($(_table[i]).find("district").text()) + '</h3><p>' + ReportName + '</p></div>';
                    div += '<div class="icon"><i class="ion ion-stats-bars" ></i ></div>' + isDetail+'</div ></div>';
                }
                if (rptid == '9') {
                    div += '<div class="col-lg-3 col-6">';
                    div += '<div class="small-box bg-warning">';
                    div += '<div class="inner"><h3>' + $.trim($(_table[i]).find("booth").text()) + '</h3><p>' + ReportName + '</p></div >';
                    div += '<div class="icon"><i class="fas fa-hotel" ></i ></div >' + isDetail+'</div ></div>';
                }
                if (rptid == '10') {
                    div += '<div class="col-lg-3 col-6">';
                    div += '<div class="small-box bg-danger">';
                    div += '<div class="inner"><h3>' + $.trim($(_table[i]).find("PendingCount").text()) + '</h3><p>' + ReportName + '</p></div >';
                    div += '<div class="icon"><i class="fas fa-hand-point-up" ></i ></div>' + isDetail+'</div ></div>';
                }
            }
            $('#4').html(div);
           
        },
        error: function (data, status, messege) {
            alert(messege);
           // $('.preloader-wrap').css('display', 'none');
        }
    });
}

function TopPanelDetail(childRptID, newsegmentId) {
    //alert(childRptID);
    $('.mymodel1').modal('show');

    var Date = _selectDate; // $.trim($('#txtdate').val());
    var SegmentID = 0;
    var ReportID = childRptID; var p_parameter_one = 0; p_parameter_two = 0; var p_parameter_three = 0; var p_parameter_four = ''; var p_parameter_five = '';

    //  $('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "',ReportID:'" + ReportID + "',p_parameter_one:'" + p_parameter_one + "',p_parameter_two:'" + p_parameter_two + "',p_parameter_three:'" + p_parameter_three + "',p_parameter_four:'" + p_parameter_four + "',p_parameter_five:'" + p_parameter_five + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var div = '';
            var Table = xml.find("Table");
            console.log(xml);
            //console.log(Table);

            $('#tbl_modal').html('');
            var finalheaderarr = []; var header = []; var _data = [];
            var headerarr = [];
            $(xml).find("Table").each(function (index, val) {
                var _header_new = $.trim($(this).find("header").text());
                var _paramValue_new = $.trim($(this).find("paramValue_one").text());
                var _ChildRptId = $.trim($(this).find("ChildRptID").text());
                var _ParamId = $.trim($(this).find("paramid").text());

                var _this = this;
                var paramid_index;

                var aaa = $(this).children();
                $(aaa).each(function (ind, value) {
                    var __this = this;
                    var nodename = value["nodeName"];
                    
                    if (nodename == "paramid") {
                        paramid_index = ind;
                    }
                    if (ind > paramid_index) {
                        var ccc = value["innerHTML"];
                        if (_header_new == "1") {
                            header.push(ccc);
                        }
                        else {
                            _data.push(ccc);
                        }
                    }
                   
                });


             
                

              // // headerarr = [_header_new, _paramValue_new];
              //  headerarr.push({ "headerID": _header_new, "value1": _paramValue_new, "ChildRptID": _ChildRptId, "ParamID": _ParamId });
              //  finalheaderarr = [headerarr];
              // //headerarr.push($.trim($(this).find("header").text()));
              ////  headerarr.push($.trim($(this).find("paramValue_one").text()));
            });
          // alert(finalheaderarr.length);
           // console.log(header);

            //console.log(_data);

            var datachunk = _data.chunk(header.length);
           
            var newdataarr = [];
            if (header.length > 0) {
                div += '<table id="example3" class="table table-bordered table-hover"><thead><tr>';

                if (header.length > 0) {
                $.each(header, function (index, value) {
                   
                    div += '<th>' + value + '</th>'
                });
                }
                div += '</tr></thead><tbody>';

                if (datachunk.length > 0) {
                    var divtbody = '';
                    $.each(datachunk, function (index, row) {
                        var $row = '<tr>';
                        $.each(row, function (cellindex, cell) {
                            $row+= '<td>' + cell + '</td>'
                        })

                        $row += '</tr>';

                        div += $row;
                   
                    });

                    
                }

                //finalheaderarr[0].forEach(function (e) {
                //    if (e.headerID == 1) {
                //        div += '<table id="example3" class="table table-bordered table-hover"><thead><tr>';
                //        div += '<th style="display:none;">paramId</th><th>' + e.value1 + '</th></tr></thead>';
                //       div += '<tbody>';
                //    }

                //    else if (e.headerID == 0) {
                //        newdataarr.push(e.value1);
                //        var subdet = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value1 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">'+e.value1+'</a>';
                //        div += '<tr><td style="display:none;"></td>';
                //        div += '<td>' + subdet+ '</td></tr>';
                //    }
                //});

                div += '</tbody></table>';
            } else {
                $('.mymodel1').modal('toggle');
                alert("No data found!!Contact to Administrator");
               
            }
            $('#tbl_modal').html(div);

            $("#example3").DataTable({

                //'columnDefs': [
                //    //hide the second & fourth column
                //    { 'visible': false, 'targets': [0] }
                //],
                pageLength: 10,
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');
           // $("#example3").DataTable(0)
            //$(xml).find("Table").each(function () {
            //    console.log($.trim($(this).find("header").text()));
            //    if ($.trim($(this).find("header").text()) == 1)
            //    {

            //        div += '<table id="example3" class="table table-bordered table-hover"><thead><tr>';
            //        div += '<th style="display:none;">paramId</th><th>' + $.trim($(Table).find("paramValue_one").text()) + '</th></tr></thead>';
            //    }
            //    else if ($.trim($(this).find("header").text()) !=1)
            //    {
            //        div += '<tbody><tr><td style="display:none;">' + $.trim($(Table).find("paramid").text()) + '</td>';
            //        div += '<td>' + $.trim($(Table).find("paramValue_one").text()) + '</td></tr></tbody></table>';
            //    }
            //})
           

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    });

}

function GetSubTable(childrID,paramId) {
    //alert(childrID);
    //alert(paramId);

    var Date = _selectDate; //$.trim($('#txtdate').val());
    var SegmentID = 0;
    var ReportID = childrID; var p_parameter_one = paramId;//paramId or 1
    var p_parameter_two = 0; var p_parameter_three = 0; var p_parameter_four = ''; var p_parameter_five = '';

    //  $('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "',ReportID:'" + ReportID + "',p_parameter_one:'" + p_parameter_one + "',p_parameter_two:'" + p_parameter_two + "',p_parameter_three:'" + p_parameter_three + "',p_parameter_four:'" + p_parameter_four + "',p_parameter_five:'" + p_parameter_five + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var div = '';
            var Table = xml.find("Table");
            $('#tbl_modal').html('');
            var finalheaderarr1 = [];
            var headerarr1 = [];
            $(xml).find("Table").each(function () {
                var _header_new1 = $.trim($(this).find("header").text());
                var _paramValue_1 = $.trim($(this).find("paramValue_one").text());
                var _paramValue_2 = $.trim($(this).find("paramValue_2").text());
                var _paramValue_3 = $.trim($(this).find("paramValue_3").text());
                var _paramValue_4 = $.trim($(this).find("paramValue_4").text());
                var _paramValue_5 = $.trim($(this).find("paramValue_5").text());
                var _paramValue_6 = $.trim($(this).find("paramValue_6").text());
                var _paramValue_7 = $.trim($(this).find("paramValue_7").text());
                var _paramValue_8 = $.trim($(this).find("paramValue_8").text());
                var _paramValue_9 = $.trim($(this).find("paramValue_9").text());
                var _paramValue_10 = $.trim($(this).find("paramValue_10").text());
                var _paramValue_11 = $.trim($(this).find("paramValue_11").text());
                var _paramValue_12 = $.trim($(this).find("paramValue_12").text());
                var _paramValue_13 = $.trim($(this).find("paramValue_13").text());
                var _ChildRptId1 = $.trim($(this).find("ChildRptID").text());
                var _ParamId1 = $.trim($(this).find("paramid").text());
               // headerarr = [_header_new, _paramValue_new];
                headerarr1.push({
                    "headerID": _header_new1, "value1": _paramValue_1, "value2": _paramValue_2, "value3": _paramValue_3, "value4": _paramValue_4, "value5": _paramValue_5,
                    "value6": _paramValue_6, "value7": _paramValue_7, "value8": _paramValue_8, "value9": _paramValue_9, "value10": _paramValue_10, "value11": _paramValue_11,
                    "value12": _paramValue_12, "value13": _paramValue_13, "ChildRptID": _ChildRptId1, "ParamID": _ParamId1
                });
                finalheaderarr1 = [headerarr1];
               //headerarr.push($.trim($(this).find("header").text()));
              //  headerarr.push($.trim($(this).find("paramValue_one").text()));
            });
          // alert(finalheaderarr.length);
            var newdataarr1 = [];
            if (finalheaderarr1.length > 0) {
                finalheaderarr1[0].forEach(function (e) {
                    if (e.headerID == 1) {
                        div += '<table id="example4" class="table table-bordered table-hover" style="overflow-x:auto;"><thead><tr>';
                        div += '<th>' + e.value1 + '</th><th>' + e.value2 + '</th><th>' + e.value3 + '</th><th>' + e.value4 + '</th><th>' + e.value5 + '</th>';
                        div += '<th>' + e.value6 + '</th><th>' + e.value7 + '</th><th>' + e.value8 + '</th><th>' + e.value9+ '</th><th>' + e.value10 + '</th>';
                        div += '<th>' + e.value11 + '</th><th>' + e.value12 + '</th><th>' + e.value13 + '</th></tr></thead>';
                       div += '<tbody>';
                    }

                    else if (e.headerID == 0) {
                        //newdataarr1.push(e.value1);
                        var subdet_1 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value1 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value1 + '</a>';
                        var subdet_2 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value2 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value2 + '</a>';
                        var subdet_3 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value3 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value3 + '</a>';
                        var subdet_4 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value4 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value4 + '</a>';
                        var subdet_5 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value5 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value5 + '</a>';
                        var subdet_6 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value6 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value6 + '</a>';
                        var subdet_7 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value7 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value7 + '</a>';
                        var subdet_8 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value8 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value8 + '</a>';
                        var subdet_9 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value9 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value9 + '</a>';
                        var subdet_10 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value10 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value10 + '</a>';
                        var subdet_11 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value11 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value11 + '</a>';
                        var subdet_12 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value12 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value12 + '</a>';
                        var subdet_13 = e.ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;color:#000000">' + e.value13 + ' </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="GetSubTable(' + e.ChildRptID + ',' + e.ParamID + ')">' + e.value13 + '</a>';
                        div += '<tr>';
                        div += '<td>' + subdet_1 + '</td><td>' + subdet_2 + '</td><td>' + subdet_3 + '</td><td>' + subdet_4 + '</td><td>' + subdet_5 + '</td>';
                        div += '<td>' + subdet_6 + '</td><td>' + subdet_7 + '</td><td>' + subdet_8 + '</td><td>' + subdet_9 + '</td><td>' + subdet_10 + '</td>';
                        div += '<td>' + subdet_11 + '</td><td>' + subdet_12 + '</td><td>' + subdet_13 + '</td></tr>';
                    }
                });

                div += '</tbody></table>';
            } else {
                $('.mymodel1').modal('toggle');
                alert("No data found!!Contact to Administrator");
               
            }
            $('#tbl_modal').html(div);
            $("#example4").DataTable({

                'columnDefs': [
                    //hide the second & fourth column
                    { 'visible': false, 'targets': [0] }
                ],
                pageLength: 10,
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example4_wrapper .col-md-6:eq(0)');

           // $("#example3").DataTable(0)
            //$(xml).find("Table").each(function () {
            //    console.log($.trim($(this).find("header").text()));
            //    if ($.trim($(this).find("header").text()) == 1)
            //    {

            //        div += '<table id="example3" class="table table-bordered table-hover"><thead><tr>';
            //        div += '<th style="display:none;">paramId</th><th>' + $.trim($(Table).find("paramValue_one").text()) + '</th></tr></thead>';
            //    }
            //    else if ($.trim($(this).find("header").text()) !=1)
            //    {
            //        div += '<tbody><tr><td style="display:none;">' + $.trim($(Table).find("paramid").text()) + '</td>';
            //        div += '<td>' + $.trim($(Table).find("paramValue_one").text()) + '</td></tr></tbody></table>';
            //    }
            //})
           

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    });
}

function BindRightNotification() {
    var Date = _selectDate; // $.trim($('#txtdate').val());
    var SegmentID = 5;

    //$('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var cnt = 0;

            var dynamicColors = function () {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };


            var div = '';
            //div += '<div class="connection-widget">';
            //div += '<div class="widget">';
            //div += '<div class="widget-people">';
            //div += '<div style="background-color:#dc3546; padding: 5px 1px 0px 10px;"><h2 class="text-body text-bold mb-4" style="color:white;">Alert</h2></div>';
            //div += '<div class="people-list">';

            $("#divSegment5").html('');
            $(xml).find("Table").each(function () {
                var boothname = $.trim($(Table).find("boothname").text());
                var date_format = $.trim($(this).find("date_format").text());
                var activitytype = $.trim($(this).find("activitytype").text());
                var activitydetailtype = $.trim($(this).find("activitydetailtype").text());
                var activityvalue = $.trim($(this).find("activityvalue").text());
                var activitytypeid = $.trim($(this).find("activitytypeid").text());

                div += '<li class="item">';
                div += '<div class="product-img">';
                div += '<span style="font-size: xx-large; font-weight:bold; color: rgb(82 160 240)">' + activityvalue + '</span>';
                div += '</div>';
                div += '<div class="product-info"><a href = "javascript:void(0)" onclick="modalopen(' + activitytypeid + ')" class="product-title" >' + activitytype + '';
                div += '<span class="product-description">' + activitydetailtype + '</span></div > </li>'
                cnt++;
            });
            //div += '</div>';
            //div += '</div>';
            //div += '</div>';
            //div += '</div>';
            $("#divSegment5").html(cnt > 0 ? div : '');


         



           // $('.preloader-wrap').css('display', 'none');
        },
        error: function (data, status, messege) {
            alert(messege);
           // $('.preloader-wrap').css('display', 'none');
        }
    });
}

var arrname = [];
var arrtimedate = [];
var arrvotedata = [];


var arrchartdata = {};
function BindSegmentClick(_SegmentID) {
    var Date = _selectDate;// $.trim($('#txtdate').val());
    var SegmentID = $.trim(_SegmentID);

  //  $('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var arreachTableGroup = []; var arreachTableXtitleGroup = []; var arreachTableYtitleGroup = [];
            var arrlabel = ['Completed', 'Pending'];
            var arrdata = []; var arrcoloR = ['#208eff', '#ffa55c'];

            var div = "";
            //$("#tab_").html('');
            $('.tabclass').html('');
            //$("#tab_2").html('');
            //$("#tab_" + SegmentID).html('');
            
            div += '<div class="row">';
            var _globalchrt = '';
            var _globalreportnam= '';

            $(xml).find("Table").each(function (index) {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var Xaxis = $.trim($(this).find("xAxis").text());
                var Yaxis = $.trim($(this).find("yAxis").text());
                var Timeset = $.trim($(this).find("timeset").text());
                var votecount = $.trim($(this).find("votecount").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var Header_one = $.trim($(this).find("Header_one").text());
                var Header_2 = $.trim($(this).find("Header_2").text());
             
                _globalchrt = ChartType;
                _globalreportnam = ReportName;


                if (ChartType == 1) {
                    arrlabel.length = 0;
                    arrlabel = [Header_one, Header_2];
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");
                   
                }
                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 "><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important;">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (Header_one == "" ? "" : Header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }
                if (_globalchrt == 2) {
           
                    
                    arrtimedate.push([index, Timeset] );
                    arrvotedata.push([index,votecount]);



                    arrname.push([Timeset, votecount])
                   // arrname.push({ x: { title: Xaxis, value: Timeset }, y: { title: Yaxis, value: votecount }});
                }
               // console.log(arrname);
            });

        
            if (_globalchrt == 2) {
        
            div += '<div class="col-md-8"><div class="card card-widget widget-user-2 barchart"><div class="widget-user-header bg-info" style="">';
                div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important;">' + _globalreportnam + '</h4></div>';
            //div += `<div class="item-icon">`;
            //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
            div += '<div class="card-footer p-0">';
            div += "<div class='card-body'><div class='barcht' id='Barchart" + SegmentID + "rpt_1' style = 'height: 153px;' ></div ></div >";
            div += "</div></div>";
            div += "</div>";
    }



            $(xml).find("Table1").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important;">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }
                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 "><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important;">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }

            })


            $(xml).find("Table2").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important;">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2"><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }

            })

            $(xml).find("Table3").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }
                
                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 "><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }

            })
            $(xml).find("Table4").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 "><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }

            })

            $(xml).find("Table5").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 "><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }

            })
            $(xml).find("Table6").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {

                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 "><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                    //div += '<div class="col-lg-4 col-6" >';
                    //div += '<div class="small-box" style="height:290px;background-color: #4b76a5 !important;">';
                    //div += '<div class="inner"><h3 style="color:white">' + completedCnt + '</h3><p style="color:white">' + ReportName + '</p></div>';
                    //div += '<div class="icon"><i class="ion ion-stats-bars" ></i ></div><a href="#" class="small-box-footer" style="margin-top:145px;" >More info <i class="fas fa-arrow-circle-right"></i></a></div ></div>';

                }

            })
            $(xml).find("Table7").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 "><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }

            })
            $(xml).find("Table8").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2"><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";


                }

            })
            $(xml).find("Table9").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2"><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";


                }

            })
            $(xml).find("Table10").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2"><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";


                }

            })
            $(xml).find("Table11").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2"><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }

            })
            $(xml).find("Table12").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("paramValue_one").text());
                var PendingCount = $.trim($(this).find("paramValue_2").text());
                var ChartType = $.trim($(this).find("chartType").text());
                var completecount = $.trim($(this).find("completedCnt").text());
                var header_one = $.trim($(this).find("Header_one").text());

                if (ChartType == 1) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2 dognutchart"><div class="widget-user-header bg-warning" style="">';
                    div += '<h4 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h4></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                    div += "<div class='card-body'><canvas id='DonutChart__" + SegmentID + "rpt_" + rptid + "' style = 'min-height: 150; height: 150; max-height: 150; max-width: 100 %;' ></canvas></div>";
                    div += "</div></div>";
                    div += "</div>";

                    var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");

                }

                else if (ChartType == 5) {
                    div += '<div class="col-md-4"><div class="card card-widget widget-user-2"><div class="widget-user-header bg-warning" style="">';
                    div += '<h5 class="widget-user-username" style="text-align: justify;margin-left:0px!important;font-size:17px!important;font-weight:500!important">' + ReportName + '</h5></div>';
                    //div += `<div class="item-icon">`;
                    //div += "<div style='margin-bottom: 10px;border-bottom: 1px solid #e4e4e4;'><h6 class='item-title' style='color:#ffffff; font-size: 10px;'>" + ReportName + "</h6></div>";
                    div += '<div class="card-footer p-0">';
                    div += "<div class='card-body align-items-center d-flex justify-content-center' style='height:190px;'><center><div class='inner'><h1 style='font-weight:700;'><span class='counter'>" + completedCnt + "</span></h1><p style='font-weight:700;'>" + (header_one == "" ? "" : header_one) + "</p></div></center></div>";
                    div += "</div></div>";
                    div += "</div>";

                }

            })
            div +='</div>'
            $("#tab_" + SegmentID).html(div);

            $.each($('.dognutchart'), function (key, value) {
                arrdata = [];
                var cnt = $(this).find('input[type=hidden]').val().split('#');
                var completedCnt = cnt[0];
                var PendingCount = cnt[1];
                arrdata = [completedCnt, PendingCount];
                var canvaid = $(this).find('canvas').attr('id');
                dougnutcall($('#' + canvaid + ''), arrlabel, arrdata, arrcoloR);
            });


            $.each($('.barchart'), function (key, value) {
                arrdata = [];
              
                arrdata = arrname;
                var canvaid = $(this).find('.barcht').attr('id');
                Barchartcall($('#' + canvaid + ''), arrname,arrvotedata,arrtimedate);
            });

            $('.counter').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                        duration: 4000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
            });


            //$('.preloader-wrap').css('display', 'none');



        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}



function dougnutcall(pietype,lebeldata,datachat,colour) {

    var donutChartCanvas = pietype.get(0).getContext('2d')
    var donutData = {
        labels: lebeldata,
        datasets: [
            {
                data: datachat,
                backgroundColor: colour,
            }
        ]
    }
    var donutOptions = {
        maintainAspectRatio: false,
        responsive: true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
        type: 'doughnut',
        data: donutData,
        options: donutOptions
    })

}

function Barchartcall(pietype, datachat,votedata,timedata) {
    var dataset = [{ label: "Votecount", data: votedata, color: "#5482FF" }];
   
    var bar_data = {
        label: "Votecount",
        data: votedata,
        bars: { show: true }
    }
    $.plot(pietype, dataset, {
        grid: {
            hoverable: true,
            borderWidth: 1,
            borderColor: '#f3f3f3',
            tickColor: '#f3f3f3',
            backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
        },
        series: {
            bars: {
                show: true, barWidth: 0.5, align: 'center',
            },
        },
        colors: ['#3c8dbc'],
        xaxis: {
            axisLabel: "Time",
            axisLabelUseCanvas: true,
            ticks: timedata
        },
        yaxis: {
            axisLabel: "Vote Count",
            axisLabelUseCanvas: true,
            tickFormatter: function (v, axis) {
                return v + "votes";
            }
        }
    })

}

//function 




function modalopen(activitytypeid) {

 //   alert(activitytypeid);
    $('.mymodel').modal('show');
    //$('#headnm').html(activityvalue);
    var Date = _selectDate;//$.trim($('#txtdate').val());
    var activitytypeid = $.trim(activitytypeid);;

    //$('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', ActivityID:'" + activitytypeid + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/AdminHome/Datagridactivity',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var cnt = 0;

            var div = '';
            //div += '<div class="connection-widget">';
            //div += '<div class="widget">';
            //div += '<div class="widget-people">';
            //div += '<div style="background-color:#dc3546; padding: 5px 1px 0px 10px;"><h2 class="text-body text-bold mb-4" style="color:white;">Alert</h2></div>';
            //div += '<div class="people-list">';

          

            div += '<table id="example2" class="table table-bordered table-hover" style="overflow-x:auto;"><thead><tr>';
            div += '<th>DistrictName</th><th>ConstituencyName</th><th>BoothName</th><th>ActivityType</th><th>ActivityDetailType</th><th>ActivityValue</th></tr></thead><tbody>';
            $(xml).find("Table").each(function () {
                var Dist = $.trim($(this).find("districtname").text());
                var constituency = $.trim($(this).find("constituencyname").text());
                var boothname = $.trim($(this).find("boothname").text());
                var activitydetailtype = $.trim($(this).find("activitydetailtype").text());
                var activityvalue = $.trim($(this).find("activityvalue").text());
                var activitytype = $.trim($(this).find("activitytype").text());

                div += '<tr><td>' + Dist+'</td>';
                div += '<td>' + constituency +'</td>'; 
                div += '<td>' + boothname + '</td>'; 
                div += '<td>' + activitydetailtype + '</td>'; 
                div += '<td>' + activitytype + '</td>';
                div += '<td>' + activityvalue +'</td></tr>'; 
            });
            div += '</tbody><table>';
            //div += '</div>';
            //div += '</div>';
            //div += '</div>';
            
            $("#tbldt").html(div);
            $("#example2").DataTable({
                pageLength: 5,
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    });
}


    function Menubindreprt(segment, childrpt) {
        //alert(childRptID);
        $('.mymodel1').modal('show');

        var Date = _selectDate; // $.trim($('#txtdate').val());
        var SegmentID = segment;
        var ReportID = childrpt; var p_parameter_one = 0; p_parameter_two = 0; var p_parameter_three = 0; var p_parameter_four = ''; var p_parameter_five = '';

        //  $('.preloader-wrap').css('display', '');
        var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "',ReportID:'" + ReportID + "',p_parameter_one:'" + p_parameter_one + "',p_parameter_two:'" + p_parameter_two + "',p_parameter_three:'" + p_parameter_three + "',p_parameter_four:'" + p_parameter_four + "',p_parameter_five:'" + p_parameter_five + "'}";
        $.ajax({
            type: 'post',
            contentType: 'application/json',
            data: A,
            dataType: 'json',
            url: '/Home/SegmentInsidedata',
            success: function (responce) {
                //console.log(responce);
                var xmlDoc = $.parseXML(responce.Data);
                var xml = $(xmlDoc);
                var div = '';
                var Table = xml.find("Table");
                $('#tbl_modal').html('');
                var finalheaderarr = [];
                var headerarr = [];
                var header = []; var _data = [];
                var _ChildRptId = '';
                var _ParamId = '';
                var prmarr = [];
                var Rptname = '';
                $(xml).find("Table").each(function (index, val) {
                    var _header_new = $.trim($(this).find("header").text());
                    Rptname = $.trim($(this).find("ReportName").text());
                    var _paramValue_new = $.trim($(this).find("paramValue_one").text());
                  _ChildRptId = $.trim($(this).find("ChildRptID").text());
                   _ParamId = $.trim($(this).find("paramid").text());
                   // var isDetail = ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;">  </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="TopPanelDetail(' + _ChildRptId + ',' + _ParamId + ')">More info <i class="fas fa-arrow-circle-right"></i></a>';
                    var _this = this;
                    var paramid_index;

                    var aaa = $(this).children();
                    $(aaa).each(function (ind, value) {
                        var __this = this;
                        var nodename = value["nodeName"];

                        if (nodename == "paramid") {
                            paramid_index = ind;
                        }
                        if (ind > paramid_index) {
                            var ccc = (value["innerHTML"] == null ? "" : value["innerHTML"]);
                            
                            if (_header_new == "1") {
                                header.push(ccc);
                            }
                            else {
                                _data.push(ccc);
                            }
                        }

                    });

                    prmarr.push(_ParamId);
           
                });


             
              //  console.log(prmarr);

                var _paramarr = prmarr.slice(1);
                //console.log(header);

                var newdatachunk = _data.chunk(header.length);
                // alert(finalheaderarr.length);
                var newdataarr = [];
                if (header.length > 0) {
                    div += '<table id="example6" class="table table-bordered table-hover"><thead><tr>';

                    if (header.length > 0) {
                        $.each(header, function (index, value) {
                            //alert(index + ": " + value);
                            div += '<th>' + value + '</th>'
                        });
                    }
                    div += '</tr></thead><tbody>';

                    if (newdatachunk.length > 0) {
                        var divtbody = '';
                        $.each(newdatachunk, function (index, row) {
                           // $.each(_paramarr, function (i, v) {
                            var $row = '<tr>';
                            $.each(row, function (cellindex, cell) {

                                $row += '<td onclick="opensubrpt(' + SegmentID+',' + ReportID +',' + _ChildRptId + ',' + _ParamId + ')">' + (cell==null?"":cell) + '</td>'
                            })

                            $row += '</tr>';

                            div += $row;
                        //    });
                        });

                    }


                    div += '</tbody></table>';
                } else {
                  
                    alert("No data found!!");
                    $('.mymodel1').modal('toggle');
                }
                
                $('#tbl_modal').html(div);
                $('.modal-title').html(Rptname);

                $("#example6").DataTable({
                    pageLength: 10,
                    "responsive": true, "lengthChange": false, "autoWidth": false,
                    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
                }).buttons().container().appendTo('#example6_wrapper .col-md-6:eq(0)');

                //$("#example6 th").each(function () {
                //    var emptyheader = $.trim($(this).text());
                //    if (emptyheader.length == 0) {
                //        $(this).hide();
                //        $("#example6 th td").each(function () {
                //            var emptybody = $.trim($(this).text());
                //            if (emptybody.length == 0) {
                //                $(this).parent().hide();
                //            }
                //        });
                //    }
                //});


            },
            error: function (data, status, messege) {
                alert(messege);
                // $('.preloader-wrap').css('display', 'none');
            }
        });

    
}

function opensubrpt(segmentid ,rptid,childreport,pid) {

    
    $('.mymodel1').modal('show');
    $('#tbl_modal').html('');
    var Date = _selectDate; // $.trim($('#txtdate').val());
    var SegmentID = segmentid;
    var ReportID = childreport; var p_parameter_one = pid; p_parameter_two = 0; var p_parameter_three = 0; var p_parameter_four = ''; var p_parameter_five = '';

    //  $('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "',ReportID:'" + ReportID + "',p_parameter_one:'" + p_parameter_one + "',p_parameter_two:'" + p_parameter_two + "',p_parameter_three:'" + p_parameter_three + "',p_parameter_four:'" + p_parameter_four + "',p_parameter_five:'" + p_parameter_five + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var div = '';
            var Table = xml.find("Table");
            $('#tbl_modal').html('');
            var finalheaderarr = [];
            var headerarr = [];
            var header = []; var _data = [];
            var _ChildRptId = '';
            var _ParamId = '';
            var prmarr = [];
            var Rptname = '';
            $(xml).find("Table").each(function (index, val) {
                var _header_new = $.trim($(this).find("header").text());
                var _paramValue_new = $.trim($(this).find("paramValue_one").text());
                Rptname = $.trim($(this).find("ReportName").text());
                _ChildRptId = $.trim($(this).find("ChildRptID").text());
                _ParamId = $.trim($(this).find("paramid").text());
                // var isDetail = ChildRptID == "0" ? '<a href="javascript:void(0)" class="small-box-footer" style="cursor:default;">  </a>' : '<a href="javascript:void(0);" class="small-box-footer"  onclick="TopPanelDetail(' + _ChildRptId + ',' + _ParamId + ')">More info <i class="fas fa-arrow-circle-right"></i></a>';
                var _this = this;
                var paramid_index;

                var aaa = $(this).children();
                $(aaa).each(function (ind, value) {
                    var __this = this;
                    var nodename = value["nodeName"];

                    if (nodename == "paramid") {
                        paramid_index = ind;
                    }
                    if (ind > paramid_index) {
                        var ccc = (value["innerHTML"] == null ? "" : value["innerHTML"]);

                        if (_header_new == "1") {
                            header.push(ccc);
                        }
                        else {
                            _data.push(ccc);
                        }
                    }

                });

                prmarr.push(_ParamId);

            });

            console.log(prmarr);

            var _paramarr = prmarr.slice(1, -1);
            //console.log(header);

            var newdatachunk = _data.chunk(header.length);
            // alert(finalheaderarr.length);
            var newdataarr = [];
            if (header.length > 0) {
                div += '<table id="example9" class="table table-bordered table-hover"><thead><tr>';

                if (header.length > 0) {
                    $.each(header, function (index, value) {
                        //alert(index + ": " + value);
                        div += '<th>' + value + '</th>'
                    });
                }
                div += '</tr></thead><tbody>';

                if (newdatachunk.length > 0) {
                    var divtbody = '';
                    $.each(newdatachunk, function (index, row) {
                        var $row = '<tr>';
                        $.each(row, function (cellindex, cell) {

                            $row += '<td onclick="opensubrpt(' + SegmentID + ',' + ReportID + ',' + _ChildRptId + ',' + _ParamId + ')">' + (cell == null ? "" : cell) + '</td>'
                        })

                        $row += '</tr>';

                        div += $row;

                    });

                }


                div += '</tbody></table>';
            } else {
               
                alert("No data found!!");
                $('.mymodel1').modal('toggle');
            }

            $('#tbl_modal').html(div);
            $('.newrpt').html(Rptname);

            $("#example9").DataTable({
                pageLength: 10,
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example6_wrapper .col-md-6:eq(0)');

            //$("#example6 th").each(function () {
            //    var emptyheader = $.trim($(this).text());
            //    if (emptyheader.length == 0) {
            //        $(this).hide();
            //        $("#example6 th td").each(function () {
            //            var emptybody = $.trim($(this).text());
            //            if (emptybody.length == 0) {
            //                $(this).parent().hide();
            //            }
            //        });
            //    }
            //});


        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    });
}


function Fact_sheetRpt() {

    //   alert(activitytypeid);
    cleardata();
    $('.factsheet').modal('show');
    $('.facthdr').html('Factsheet');
    var Date = _selectDate;//$.trim($('#txtdate').val());
    
}


function navbarclose() {
    $(".nav-treeview").css({ "display": "none"});

}



function get_a_constituency_autocomp(pcid,distID) {
    var collectiondata = [];
    var A = "{transname: 'load_a_constituency', DistrictId:'" + distID + "',ContituencyID:'',BoothID:'',PCID:'" + pcid+"'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            $.each(Table, function (i, v) {
                var node = {

                    label: $(v).find('constituencyname').text(),
                    value: $(v).find('constituencyID').text()

                };

                collectiondata.push(node);
            })

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    $("#txt_Constituency").autocomplete({

        source: collectiondata,
      
        select: function (e, i) {
            $('#txt_Constituency').val(i.item.label);
            $('#constituency_id').val(i.item.value);
            get_booth_autocomp(i.item.value)
            e.preventDefault();
        },
        minLength: 0
    }).focus(function () {
        $(this).autocomplete('search', $(this).val())
    });
}


function get_constituency_autocomp(distID) {
    var collectiondata = [];
    var A = "{transname: 'load_p_constituency', DistrictId:'" + distID + "',ContituencyID:'',BoothID:''}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            $.each(Table, function (i, v) {
                var node = {

                    label: $(v).find('PCName').text(),
                    value: $(v).find('PCID').text()

                };

                collectiondata.push(node);
            })

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    $("#txt_parlcon").autocomplete({

        source: collectiondata,

        select: function (e, i) {
            $('#txt_parlcon').val(i.item.label);
            $('#parlcon_id').val(i.item.value);
            var distid = $('#dist_id').val();
            get_a_constituency_autocomp(i.item.value, distid)
            e.preventDefault();
        },
        minLength: 0
    }).focus(function () {
        $(this).autocomplete('search', $(this).val())
    });
}

function get_dist_autocomp() {
    var collectiondata = [];
    var A = "{transname: 'load_district', DistrictId:'',ContituencyID:'',BoothID:''}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            $.each(Table, function (i, v) {
                var node = {

                    label: $(v).find('districtname').text(),
                    value: $(v).find('districtID').text()

                };

                collectiondata.push(node);
            })

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    $("#txt_district").autocomplete({
       
        source: collectiondata,
       
        select: function (e, i) {
            $('#txt_district').val(i.item.label);
            $('#dist_id').val(i.item.value);
            get_constituency_autocomp(i.item.value);
            e.preventDefault();
        },
        minLength: 0
    }).focus(function () {
        $(this).autocomplete('search', $(this).val())
    });
}

function get_booth_autocomp(contID) {
    var collectiondata = [];
    var A = "{transname: 'load_booth', DistrictId:'',ContituencyID:'" + contID +"',BoothID:''}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            $.each(Table, function (i, v) {
                var node = {

                    label: $(v).find('boothname').text(),
                    value: $(v).find('boothID').text()

                };

                collectiondata.push(node);
            })

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    $("#txt_booth").autocomplete({

        source: collectiondata,
       
        select: function (e, i) {
            $('#txt_booth').val(i.item.label);
            $('#booth_id').val(i.item.value);
           // load_report();
            e.preventDefault();
        },
        minLength: 0
    }).focus(function () {
        $(this).autocomplete('search', $(this).val())
    });
}

var str1 = '';
var str2 = '';
var str3 = '';
var str4 = '';
var str5 = '';
var str6 = '';

function demo() {
    str1 = '';
    str2 = '';
    str3 = '';
    str4 = '';
    str5 = '';
    str6 = '';

    var boothid = $('#booth_id').val();
    var Date = _selectDate; 

    $('.democard').css('display', 'block');
    $('#demo1').html('');
    var _tablecollection = [];
    var A = "{transname: 'load_report', DistrictId:'',ContituencyID:'',BoothID:'" + boothid + "' ,p_Date:'" + Date +"'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);

            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            console.log(Table)
            var tblCnt = responce.Message;// alert(tblCnt);
            //$(xml).find("Table1").each(function (index) {

            //})

            if (tblCnt > 0) {
                for (var i = 0; i <= tblCnt; i++) {
                    var table = $(xml).find('Table1');
                    console.log(table);
                    var header = []; var _data = []; var top_header_new = ''; var accord = '';
                    $(xml).find('Table' + i).each(function () {
                        var _header_new = $.trim($(this).find("isHeader").text());
                        top_header_new = $.trim($(this).find("top_header").text());
                        accord = $.trim($(this).find("accordian").text());
                        var paramid_index;
                        var aaa = $(this).children();
                        $(aaa).each(function (ind, value) {
                            var __this = this;
                            var nodename = value["nodeName"];

                            if (nodename == "paramid") {
                                paramid_index = ind;
                            }
                            if (ind > paramid_index) {
                                var ccc = value["innerHTML"];
                                if (_header_new == "1") {
                                    header.push(ccc);
                                }
                                else {
                                    _data.push(ccc);
                                }
                            }

                        });

                    });
                     console.log(header);
                     console.log(_data);
                    var div = '';
                    div += '<div class="col-12"><div class="card" id="card_' + i + '"><div class="card-header fctheader" id="cardh_' + i + '"><h3 class="card-title "><center><h3><span id="headerdata_' + i + '">' + top_header_new + '</span></h3></center></h3>'
                    div += '<div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse"> <i class="fas fa-minus"></i> </button> </div></div><div class="card-body">';
                    div += '<table class="table table-bordered table-hover newtable" border="1" style="border-collapse:collapse;"><thead><tr>';
                    if (header.length > 0) {

                        if (header.length > 0) {
                            $.each(header, function (index, value) {
                                //alert(index + ": " + value);
                                div += '<th class="header_' + index + '">' + value + '</th>'
                            });
                        }
                        div += '</tr></thead><tbody>';

                        if (_data.length > 0) {
                            var datachunkall = _data.chunk(header.length);

                            if (datachunkall.length > 0) {
                                var divtbody = '';
                                $.each(datachunkall, function (index, row) {
                                    var $row = '<tr>';
                                    $.each(row, function (cellindex, cell) {
                                        // $("#headerdata_" + i).val(cell);
                                        $row += '<td class="bdata_' + cellindex + '">' + cell + '</td>'
                                    })

                                    $row += '</tr>';

                                    div += $row;

                                });


                            }



                        }
                        div += '</tbody></table></div></div></div>';

                        _tablecollection.push(top_header_new);
                    } else {
                        // $('.mymodel1').modal('toggle');
                        //  alert("No data found!!Contact to Administrator");

                    }
                    //str1 += div;

                    if (accord == 1) {
                        str1 += div;
                        $('#demo' + accord + '').html(str1);
                    }

                    if (accord == 2) {
                        str2 += div;
                        $('#demo' + accord + '').html(str2);
                    }

                    if (accord == 3) {
                        str3 += div;
                        $('#demo' + accord + '').html(str3);
                    }

                    if (accord == 4) {
                        str4 += div;
                        $('#demo' + accord + '').html(str4);
                    }

                    if (accord == 5) {
                        str5 += div;
                        $('#demo' + accord + '').html(str5);
                    }

                    if (accord == 6) {
                        str6 += div;
                        $('#demo' + accord + '').html(str6);
                    }

                    //if (accord == 1) {
                    //    $('#demo' + accord + '').html(str1);
                    //    str1 = '';
                    //}
                    //if (accord == 2) {
                    //    $('#demo' + accord + '').html(str1);
                    //    str1 = '';
                    //}
                    //if (accord == 3) {
                    //    $('#demo' + accord + '').html(str1);
                    //    str1 = '';
                    //}
                    //if (accord == 4) {
                    //    $('#demo' + accord + '').html(str1);
                    //    str1 = '';
                    //}

                   //$('#demo' + accord+'').html(str1);
                    //$.each(table, function (i, v) {
                    // bindheaderdata(table);
                    // });
                }
               // $('#headerdata_0').html(_tablecollection[0])

            }
            else {
                alert('No record found !!');
            }

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    

}





var str = '';
function load_report() {
    var _tablecollection = [];
    var A = "{transname: 'load_report', DistrictId:'',ContituencyID:'',BoothID:''}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            //console.log(responce.Data);
            //console.log(xml);

            var tblCnt = responce.Message;// alert(tblCnt);
              
            if (tblCnt > 0) {
                for (var i = 0; i <= tblCnt; i++) {
                    var table = $(xml).find('Table' + i);
                    console.log(table);
                    var header = []; var _data = []; var top_header_new = '';
                    $(xml).find('Table' + i).each(function () {
                        var _header_new = $.trim($(this).find("isHeader").text());
                        top_header_new = $.trim($(this).find("top_header").text());
                        var paramid_index;
                        var aaa = $(this).children();
                        $(aaa).each(function (ind, value) {
                            var __this = this;
                            var nodename = value["nodeName"];

                            if (nodename == "paramid") {
                                paramid_index = ind;
                            }
                            if (ind > paramid_index) {
                                var ccc = value["innerHTML"];
                                if (_header_new == "1") {
                                    header.push(ccc);
                                }
                                else {
                                    _data.push(ccc);
                                }
                            }

                        });
                        
                    });
                   // console.log(header);
                   // console.log(_data);
                    var div = '';
                    div += '<div class="col-12"><div class="card" id="card_' + i + '"><div class="card-header fctheader" id="cardh_' + i +'"><h3 class="card-title "><center><h3><span id="headerdata_' + i + '">' + top_header_new + '</span></h3></center></h3>'
                    div += '<div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse"> <i class="fas fa-minus"></i> </button> </div></div><div class="card-body">';
                    div += '<table class="table table-bordered table-hover newtable" border="1" style="border-collapse:collapse;"><thead><tr>';
                    if (header.length > 0) {

                        if (header.length > 0) {
                            $.each(header, function (index, value) {
                                //alert(index + ": " + value);
                                div += '<th class="header_'+index+'">' + value + '</th>'
                            });
                        }
                        div += '</tr></thead><tbody>';
                      
                        if (_data.length > 0) {
                            var datachunkall = _data.chunk(header.length);

                            if (datachunkall.length > 0) {
                                var divtbody = '';
                                $.each(datachunkall, function (index, row) {
                                    var $row = '<tr>';
                                    $.each(row, function (cellindex, cell) {
                                       // $("#headerdata_" + i).val(cell);
                                        $row += '<td class="bdata_' + cellindex +'">' + cell + '</td>'
                                    })

                                    $row += '</tr>';

                                    div += $row;

                                });


                            }

                        

                        }
                        div += '</tbody></table></div></div></div><br><hr/>';
                        
                        _tablecollection.push(top_header_new);
                    } else {
                        // $('.mymodel1').modal('toggle');
                        //  alert("No data found!!Contact to Administrator");

                    }
                    str += div;

                    $('#data_dynamic').html(str);
                    //$.each(table, function (i, v) {
                   // bindheaderdata(table);
                   // });
                }
                $('#headerdata_0').html(_tablecollection[0])
              
            }
            else {
                alert('No record found !!');
            }

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

}

function bindheaderdata(table) {
    var header = []; var _data = [];
    $.each(table, function (index, val) {
        

        var _header_new = $.trim($(this).find("isHeader").text());
     

        var _this = this;
        var paramid_index;

        var aaa = $(this).children();
        $(aaa).each(function (ind, value) {
            var __this = this;
            var nodename = value["nodeName"];

            if (nodename == "paramid") {
                paramid_index = ind;
            }
            if (ind > paramid_index) {
                var ccc = value["innerHTML"];
                if (_header_new == "1") {
                    header.push(ccc);
                }
                else {
                    _data.push(ccc);
                }
            }

        });
        console.log(header);
        console.log(_data);



    });

    var div = '';
    div += '<table id="example7" class="table table-bordered table-hover"><thead><tr>';
    if (header.length > 0) {
        
        if (header.length > 0) {
            $.each(header, function (index, value) {
                //alert(index + ": " + value);
                div += '<th>' + value + '</th>'
            });
        }
        div += '</tr></thead><tbody><tr>';
        if (_data.length > 0) {
            $.each(_data, function (index, value) {
                //alert(index + ": " + value);
                div += '<td>' + value + '</td>'
            });
        }
        div += '</tr></tbody></table>';
    } else {
        // $('.mymodel1').modal('toggle');
        //  alert("No data found!!Contact to Administrator");

    }
    str += div;
    $('#data_dynamic').html(str);
   

}


function printpage() {
    
        // $('.aa').css('display', '');
        var prnt = document.getElementById('data_dynamic');
        prnt.border = 1;

        prnt.align = "center";
        prnt.title = "VoteDocuments";
        var p = window.open('', 'Print', 'left=100,top=100,width=1000,height=1000; border-collapse=collapse');

       
        p.document.write(prnt.innerHTML);


        //p.document.write("<h2 style='text-align:center; '>" + "sdfsdfasdfasdfasdf" + "</h1>");
        //$("#lblHeader").show();


        p.document.close();
        p.focus();


        //$('tr').children().eq(0).show();
       // $('#tbl tr').find('td:eq(0)').show();

        p.print();
       // p.close();
        //$("#lblHeader").hide();
    
}

function cleardata() {
    $('#txt_booth').val('');
    $('#booth_id').val('');
    $('#txt_parlcon').val('');
    $('#parlcon_id').val('');
    $('#txt_district').val('');
    $('#dist_id').val('');
    $('#txt_Constituency').val('');
    $('#constituency_id').val('');
   //$('#data_dynamic').html('');
    $('.democard').css('display', 'none');
    $('.newrpt').html('');
}


function get_a_constituency_autocomp1(pcid, distID) {
    var collectiondata = [];
    var A = "{transname: 'load_a_constituency', DistrictId:'" + distID + "',ContituencyID:'',BoothID:'',PCID:'" + pcid + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            $.each(Table, function (i, v) {
                var node = {

                    label: $(v).find('constituencyname').text(),
                    value: $(v).find('constituencyID').text()

                };

                collectiondata.push(node);
            })

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    $("#txt_Constituency1").autocomplete({

        source: collectiondata,

        select: function (e, i) {
            $('#txt_Constituency1').val(i.item.label);
            $('#constituency_id1').val(i.item.value);
            get_booth_autocomp1(i.item.value)
            e.preventDefault();
        },
        minLength: 0
    }).focus(function () {
        $(this).autocomplete('search', $(this).val())
    });
}

function get_dist_autocomp1() {
    var collectiondata = [];
    var A = "{transname: 'load_district', DistrictId:'',ContituencyID:'',BoothID:''}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            $.each(Table, function (i, v) {
                var node = {

                    label: $(v).find('districtname').text(),
                    value: $(v).find('districtID').text()

                };

                collectiondata.push(node);
            })

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    $("#txt_district1").autocomplete({

        source: collectiondata,

        select: function (e, i) {
            $('#txt_district1').val(i.item.label);
            $('#dist_id1').val(i.item.value);
            get_constituency_autocomp1(i.item.value);
            e.preventDefault();
        },
        minLength: 0
    }).focus(function () {
        $(this).autocomplete('search', $(this).val())
    });
}

function get_booth_autocomp1(contID) {
    var collectiondata = [];
    var A = "{transname: 'load_booth', DistrictId:'',ContituencyID:'" + contID + "',BoothID:''}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            $.each(Table, function (i, v) {
                var node = {

                    label: $(v).find('boothname').text(),
                    value: $(v).find('boothID').text()

                };

                collectiondata.push(node);
            })

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    $("#txt_booth1").autocomplete({

        source: collectiondata,

        select: function (e, i) {
            $('#txt_booth1').val(i.item.label);
            $('#booth_id1').val(i.item.value);
            load_report();
            e.preventDefault();
        },
        minLength: 0
    }).focus(function () {
        $(this).autocomplete('search', $(this).val())
    });
}

function get_constituency_autocomp1(distID) {
    var collectiondata = [];
    var A = "{transname: 'load_p_constituency', DistrictId:'" + distID + "',ContituencyID:'',BoothID:''}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/FactSheetData',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            $.each(Table, function (i, v) {
                var node = {

                    label: $(v).find('PCName').text(),
                    value: $(v).find('PCID').text()

                };

                collectiondata.push(node);
            })

        },
        error: function (data, status, messege) {
            alert(messege);
            // $('.preloader-wrap').css('display', 'none');
        }
    })

    $("#txt_parlcon1").autocomplete({

        source: collectiondata,

        select: function (e, i) {
            $('#txt_parlcon1').val(i.item.label);
            $('#parlcon_id1').val(i.item.value);
            var distid = $('#dist_id1').val();
            get_a_constituency_autocomp1(i.item.value, distid)
            e.preventDefault();
        },
        minLength: 0
    }).focus(function () {
        $(this).autocomplete('search', $(this).val())
    });
}