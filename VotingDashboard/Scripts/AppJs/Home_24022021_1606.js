﻿$(document).ready(function () {
    $('#txtdate').mask("9999-99-99", { placeholder: "_" });
    $('#txtdate').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'yy-mm-dd',
        onSelect: function (dateText, inst) {
            BindSegment();
            BindRightNotification();
        }
    });
    $("#txtdate").datepicker().datepicker("setDate", new Date());
    Dashboard();
    BindSegment();
    BindRightNotification();
});

function BindSegment() {
    var Date = $.trim($('#txtdate').val());
    var SegmentID = $.trim($('#hdnselectedsegmentid').val());
    
    $('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
           
            var arrlabel = ['completedCnt', 'PendingCount'];
            var arrdata = []; var arrcoloR = ['#208eff', '#ffa55c'];

            var div = "";

            $("#tab_" + SegmentID + " .gutters-20").html('');
            $(xml).find("Table").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("completedCnt").text());
                var PendingCount = $.trim($(this).find("PendingCount").text());

                div += "<div class='col-md-3 crypto-pai-chart'>";
                div += "<div class='media' style='display:block;background-image: linear-gradient(#099285, #b0f1db);'>";
                //div += `<div class="item-icon">`;
                div += "<div><h6 class='item-title' style='color:#ffffff;'>" + ReportName + "</h6></div>";
                div += "<div style='height:50px'>";
                div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                div += "<canvas id='pie-charttab_" + SegmentID + "rpt_" + rptid + "' ></canvas>";
                div += "</div>";
                 //div+=`</div>`;
                //div += "<div class='media-body'>";
                //div += "<h4 class='item-title' style='color:#ffffff;'>" + ReportName + "</h4>";

                ////div += "<div class='crypto-pai-chart'>";
                
                ////div += "</div>";

                //div += "</div>";
                div += "</div>";
                div += "</div>";

                var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");
            });
            $("#tab_" + SegmentID + " .gutters-20").html(div);

            $.each($('.crypto-pai-chart'), function (key, value) {
                arrdata = [];
                var cnt = $(this).find('input[type=hidden]').val().split('#');
                var completedCnt = cnt[0];
                var PendingCount = cnt[1];
                arrdata = [completedCnt, PendingCount];
                var canvaid = $(this).find('canvas').attr('id');
                pie_chart_call($('#' + canvaid + ''), arrlabel, arrdata, arrcoloR);
            });


            //var rptid = $.trim($(Table).find("rptid").text());
            //var ReportName = $.trim($(Table).find("ReportName").text());
            //var ActivityID = $.trim($(Table).find("ActivityID").text());
            //var isDefault = $.trim($(Table).find("isDefault").text());
            //var Segment = $.trim($(Table).find("Segment").text());
            //var rpt_rank = $.trim($(Table).find("rpt_rank").text());
            //var chartType = $.trim($(Table).find("chartType").text());
            //var completedCnt = $.trim($(Table).find("completedCnt").text());
            //var PendingCount = $.trim($(Table).find("PendingCount").text());

            $('.preloader-wrap').css('display', 'none');



        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}

function BindSegmentClick(_SegmentID) {
    var Date = $.trim($('#txtdate').val());
    var SegmentID = $.trim(_SegmentID);

    $('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            var arrlabel = ['completedCnt', 'PendingCount'];
            var arrdata = []; var arrcoloR = ['#208eff', '#ffa55c'];

            var div = "";

            $("#tab_" + SegmentID + " .gutters-20").html('');
            $(xml).find("Table").each(function () {
                var rptid = $.trim($(this).find("rptid").text());
                var ReportName = $.trim($(this).find("ReportName").text());
                var completedCnt = $.trim($(this).find("completedCnt").text());
                var PendingCount = $.trim($(this).find("PendingCount").text());

                div += "<div class='col-md-3 crypto-pai-chart'>";
                div += "<div class='media' style='display:block;background-image: linear-gradient(#099285, #b0f1db);'>";
                //div += `<div class="item-icon">`;
                div += "<div><h6 class='item-title' style='color:#ffffff;'>" + ReportName + "</h6></div>";
                div += "<div style='height:50px'>";
                div += "<input type='hidden' value='" + completedCnt + '#' + PendingCount + "' />";
                div += "<canvas id='pie-charttab_" + SegmentID + "rpt_" + rptid + "' ></canvas>";
                div += "</div>";
                //div+=`</div>`;
                //div += "<div class='media-body'>";
                //div += "<h4 class='item-title' style='color:#ffffff;'>" + ReportName + "</h4>";

                ////div += "<div class='crypto-pai-chart'>";

                ////div += "</div>";

                //div += "</div>";
                div += "</div>";
                div += "</div>";

                var pie_chart = $("#pie-charttab_" + SegmentID + "rpt_" + rptid + "");
            });
            $("#tab_" + SegmentID + " .gutters-20").html(div);

            $.each($('.crypto-pai-chart'), function (key, value) {
                arrdata = [];
                var cnt = $(this).find('input[type=hidden]').val().split('#');
                var completedCnt = cnt[0];
                var PendingCount = cnt[1];
                arrdata = [completedCnt, PendingCount];
                var canvaid = $(this).find('canvas').attr('id');
                pie_chart_call($('#' + canvaid + ''), arrlabel, arrdata, arrcoloR);
            });


            $('.preloader-wrap').css('display', 'none');



        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}

function BindRightNotification() {
    var Date = $.trim($('#txtdate').val());
    var SegmentID = 5;

    $('.preloader-wrap').css('display', '');
    var A = "{myDate: '" + Date + "', SegmentID:'" + SegmentID + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/SegmentInsidedata',
        success: function (responce) {
            //console.log(responce);
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            var cnt = 0;

            var div = '';
            div += '<div class="connection-widget">';
            div += '<div class="widget">';
            div += '<div class="widget-people">';
            div += '<h6 class="text-body text-bold mb-4"></h6>';
            div += '<div class="people-list">';

            $("#divSegment5").html('');
            $(xml).find("Table").each(function () {
                var boothname = $.trim($(Table).find("boothname").text());
                var date_format = $.trim($(this).find("date_format").text());
                var activitytype = $.trim($(this).find("activitytype").text());
                var activitydetailtype = $.trim($(this).find("activitydetailtype").text());
                var activityvalue = $.trim($(this).find("activityvalue").text());
                
                div += '<a href="javascript:void(0)" class="media">';
                div += ' <div class="item-img">';
                div += '<span>' + activityvalue + '</span>';
                div += '</div>';
                div += '<div class="media-body">';
                div += '<h5 class="item-title text-bold mb-0">' + activitytype + '</h5>';
                div += '<div class="text-bombay">' + activitydetailtype + '</div>';
                div += '</div>';
                div += '</a>';
                cnt++;
            });
            div += '</div>';
            div += '</div>';
            div += '</div>';
            div += '</div>';

            $("#divSegment5").html(cnt>0?div:'');

            




            $('.preloader-wrap').css('display', 'none');
        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}

function groupByCategory(data) {
    var groups = {};
    $(data).each(function (k, v) {
        if (!(v.subcategory in groups)) {
            groups[v.subcategory] = "";
        }
        groups[v.subcategory] = groups[v.subcategory] + "<a href='' class='profile-tag'>" + v.subgroup + "</a>";
    });
    return groups;
}







function Dashboard() {
    var Date = $.trim($('#txtdate').val());
    //alert(Date); return false;
    $('.preloader-wrap').css('display', '');
    var A = "{strDate: '" + Date + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/DashboardDate',
        success: function (responce) {
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");
            //$('#divModal').modal('show');

            var District = $.trim($(Table).find("District").text());
            var Constituency = $.trim($(Table).find("Constituency").text());
            var Booth = $.trim($(Table).find("Booth").text());

            $('#divdashDistrict').text(District);
            $('#divdashConstituency').text(Constituency);
            $('#divdashBooth').text(Booth);

            $('.preloader-wrap').css('display', 'none');



        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}



function Detail(Type) {
    $('#divTable').css('display', '');
    var activityTypeID = 0;
    var activityDetailID = 0;
    var DistrictID = 0;
    var ConstituencyID = 0;
    var boothID = 0;
    var inputDate = $.trim($('#txtdate').val());

    $('.preloader-wrap').css('display', '');
    var A = ""; var actionmethodName = ""; var tblColumnData = "";
    if (Type == 'District') {
        $('#headerDeashboardDetail').text('District');
        actionmethodName = "activityDetailCountDistrictWise";
        $('#lastTH').text('District');
        A = "{activityTypeID: '" + activityTypeID + "', activityDetailID: '" + activityDetailID + "', DistrictID: '" + DistrictID + "', inputDate: '" + inputDate + "'}";
        tblColumnData = 'districtname';
    }
    if (Type == 'Constituency') {
        $('#headerDeashboardDetail').text('Constituency');
        actionmethodName = "activityDetailCountConstituencyWise";
        $('#lastTH').text('Constituency');
        A = "{activityTypeID: '" + activityTypeID + "', activityDetailID: '" + activityDetailID + "', ConstituencyID: '" + ConstituencyID + "', inputDate: '" + inputDate + "'}";
        tblColumnData = 'constituencyname';
    }
    if (Type == 'Booth') {
        $('#headerDeashboardDetail').text('Booth');
        actionmethodName = "activityDetailCountBoothWise";
        $('#lastTH').text('Booth');
        A = "{activityTypeID: '" + activityTypeID + "', activityDetailID: '" + activityDetailID + "', boothID: '" + boothID + "', inputDate: '" + inputDate + "'}";
        tblColumnData = 'boothname';
    }
    
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/' + actionmethodName,
        success: function (response) {
            if (response.Status == 200) {
                var t = response.Data; //alert(JSON.stringify(t));
                //console.log(t);
                var columnData = [{ "mDataProp": "CNT" },
                { "mDataProp": "activitytype" },
                { "mDataProp": tblColumnData }
                ];

                var columnDataHide = [];
                var oTable = $('#tbl').DataTable({
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                    ],

                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aaData": t,

                    "aoColumns": columnData,
                    "aoColumnDefs": [
                        {
                            "targets": columnDataHide,
                            "visible": false,
                            "searchable": false
                        }],
                    'iDisplayLength': 10,
                    destroy: true
                });

            } else {
                alert(response.Message);
            }


            //$('#divDistrictDetail').css('display', '');
            //$('#divConstituencyDetail').css('display', 'none');
            //$('#divBoothDetail').css('display', 'none');

            $('.preloader-wrap').css('display', 'none');

        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}

function District1() {
    $('#headerDeashboardDetail').text('District');
    $('#divBoothDetail').css('display', 'none');
    var activityTypeID = 0;
    var activityDetailID = 0;
    var DistrictID = 0;
    var inputDate = $.trim($('#txtdate').val());

    $('.preloader-wrap').css('display', '');

    var A = "{activityTypeID: '" + activityTypeID + "', activityDetailID: '" + activityDetailID + "', DistrictID: '" + DistrictID + "', inputDate: '" + inputDate + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/activityDetailCountDistrictWise',
        success: function (responce) {
            //console.log(responce)
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            $("#tblDistrict tr.trclass").remove();
            $(xml).find("Table").each(function () {
                $("#tblDistrict").append("<tr class='trclass'>" +
                        "<td class='text-body font-weight-bold'>" + $(this).find("CNT").text() + "</td>" +
                        "<td class='text-yellow'>" + $(this).find("activitytype").text() + "</td>" +
                        "<td class='text-blue-ribbon'>" + $(this).find("districtname").text() + "</td></tr>");
            });

            pie_chart_call($('#pie-chartDistrict'), arrlabel, arrdata, arrcoloR);

            $('#headerDeashboardDetail').text('District');
            $('#divDistrictDetail').css('display', '');
            $('#divConstituencyDetail').css('display', 'none');
            $('#divBoothDetail').css('display', 'none');

            $('.preloader-wrap').css('display', 'none');

        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}

function Constituency() {
    var activityTypeID = 0;
    var activityDetailID = 0;
    var ConstituencyID = 0;
    var inputDate = $.trim($('#txtdate').val());

    $('.preloader-wrap').css('display', '');

    var A = "{activityTypeID: '" + activityTypeID + "', activityDetailID: '" + activityDetailID + "', ConstituencyID: '" + ConstituencyID + "', inputDate: '" + inputDate + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/activityDetailCountConstituencyWise',
        success: function (responce) {
            //console.log(responce)
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            var arrlabel = []; var arrdata = []; var arrcoloR = [];

            var dynamicColors = function () {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            $("#tblConstituency tr.trclass").remove();
            $(xml).find("Table").each(function () {
                $("#tblConstituency").append("<tr class='trclass'>" +
                        "<td class='text-body font-weight-bold'>" + $(this).find("CNT").text() + "</td>" +
                        "<td class='text-yellow'>" + $(this).find("activitytype").text() + "</td>" +
                        "<td class='text-blue-ribbon'>" + $(this).find("constituencyname").text() + "</td></tr>");
                arrlabel.push($(this).find("activitytype").text());
                arrdata.push($(this).find("CNT").text());
                arrcoloR.push(dynamicColors());
            });
            
            pie_chart_call($('#pie-chartConstituency'), arrlabel, arrdata, arrcoloR);

            $('#headerDeashboardDetail').text('Constituency');
            $('#divDistrictDetail').css('display', 'none');
            $('#divConstituencyDetail').css('display', '');
            $('#divBoothDetail').css('display', 'none');

            $('.preloader-wrap').css('display', 'none');

        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}

function Booth() {
    var activityTypeID = 0;
    var activityDetailID = 0;
    var boothID = 0;
    var inputDate = $.trim($('#txtdate').val());

    $('.preloader-wrap').css('display', '');

    var A = "{activityTypeID: '" + activityTypeID + "', activityDetailID: '" + activityDetailID + "', boothID: '" + boothID + "', inputDate: '" + inputDate + "'}";
    $.ajax({
        type: 'post',
        contentType: 'application/json',
        data: A,
        dataType: 'json',
        url: '/Home/activityDetailCountBoothWise',
        success: function (responce) {
            //console.log(responce)
            var xmlDoc = $.parseXML(responce.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            var arrlabel = []; var arrdata = []; var arrcoloR = [];

            var dynamicColors = function () {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };

            $("#tblBooth tr.trclass").remove();
            $(xml).find("Table").each(function () {
                $("#tblBooth").append("<tr class='trclass'>" +
                        "<td class='text-body font-weight-bold'>" + $(this).find("CNT").text() + "</td>" +
                        "<td class='text-yellow'>" + $(this).find("activitytype").text() + "</td>" +
                        "<td class='text-blue-ribbon'>" + $(this).find("boothname").text() + "</td></tr>");
                arrlabel.push($(this).find("activitytype").text());
                arrdata.push($(this).find("CNT").text());
                arrcoloR.push(dynamicColors());
            });

            pie_chart_call($('#pie-chartBooth'), arrlabel, arrdata, arrcoloR);

            $('#headerDeashboardDetail').text('Booth');
            $('#divDistrictDetail').css('display', 'none');
            $('#divConstituencyDetail').css('display', 'none');
            $('#divBoothDetail').css('display', '');

            $('.preloader-wrap').css('display', 'none');

        },
        error: function (data, status, messege) {
            alert(messege);
            $('.preloader-wrap').css('display', 'none');
        }
    });
}

function pie_chart_call(pie_chart, arrlabel, arrdata, arrcoloR) {
    //console.log(pie_chart);
    //console.log(arrlabel);
    //console.log(arrdata);
    //console.log(arrcoloR);
    if (pie_chart.length) {
        var earningCanvas = pie_chart.get(0).getContext("2d");
        var earningChart = new Chart(earningCanvas, {
            type: 'pie',
            responsive: true,
            data: {
                labels: arrlabel,
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: arrcoloR,
                    data: arrdata
                }]
            },
            options: {
                    
                responsive: true,
                maintainAspectRatio: false,
                rotation: -0.05,
                title: {
                    display: false,
                    text: 'Custom Chart Title',
                    position: 'top',
                    lineHeight:1
                },
                legend: {
                    display: false,
                    position: 'bottom',
                    padding:50
                },
                layout: {
                    padding: {
                        left:0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        });
    }
}