﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dash.Models
{
    public class DashboardRPTMD
    {
        public string District { get; set; }
        public string Constituency { get; set; }
        public string Booth { get; set; }
    }
    public class DashboardMD
    {
        public int? SegmentID { get; set; }
        public string SegmentName { get; set; }
        public int? isSelected { get; set; }
        public string PanelType { get; set; }
        public string PanelSide { get; set; }
    }
    public class DetailMD
    {
        public string CNT { get; set; }
        public string activitytype { get; set; }
    }

    public class DistrictDetailMD : DetailMD
    {
        public string districtname { get; set; }
    }
    public class ConstituencyDetailMD : DetailMD
    {
        public string constituencyname { get; set; }
    }
    public class BoothDetailMD : DetailMD
    {
        public string boothname { get; set; }
    }


}