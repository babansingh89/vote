﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dash.AppCode
{
    public class ClientJsonResult
    {
        public object Data { get; set; }
        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
    }
}