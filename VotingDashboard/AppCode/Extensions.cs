﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace dash
{
    public static class Extensions
    {
        public static T GetValue<T>(this SqlDataReader dataReader, string columnName)
        {
           if(dataReader.IsDBNull(dataReader.GetOrdinal(columnName)))
            {
                return default(T);
            }else
            {
                return (T)dataReader.GetValue(dataReader.GetOrdinal(columnName));
            }
        }
    }
}